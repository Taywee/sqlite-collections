use std::{convert::Infallible, net::Ipv4Addr};

use super::*;
use crate::serializer::Direct;

#[cfg(feature = "cbor")]
use crate::serializer::Cbor;

#[cfg(feature = "json")]
use crate::serializer::Json;

#[cfg(feature = "postcard")]
use crate::serializer::Postcard;

#[test]
fn test_insert_direct() {
    let connection = Connection::open_in_memory().unwrap();
    let mut set: BTreeSet<Direct<String>, _> = BTreeSet::open(connection).unwrap();
    assert_eq!(set.first().unwrap(), None);
    assert_eq!(set.last().unwrap(), None);
    assert!(!set.contains("hello").unwrap());
    assert!(set.insert("hello").unwrap());
    assert_eq!(set.first().unwrap(), Some(String::from("hello")));
    assert_eq!(set.last().unwrap(), Some(String::from("hello")));
    assert!(set.contains("hello").unwrap());
    assert!(!set.insert(&String::from("hello")).unwrap());
    set.insert("foo").unwrap();
    set.insert("bar").unwrap();
    assert_eq!(set.first().unwrap(), Some(String::from("bar")));
    assert_eq!(set.last().unwrap(), Some(String::from("hello")));
}

#[test]
fn test_iter_order_direct() -> Result<(), Box<dyn std::error::Error>> {
    let mut connection = Connection::open_in_memory()?;
    let mut set: BTreeSet<Direct<String>, _> = BTreeSet::open(&mut connection)?;
    assert!(set.insert("alpha")?);
    assert!(set.insert("beta")?);
    assert!(set.insert("gamma")?);
    assert!(set.insert("delta")?);
    assert!(set.insert("epsilon")?);
    assert!(set.insert("zeta")?);
    assert!(set.insert("eta")?);
    assert!(set.insert("theta")?);
    assert!(set.insert("iota")?);
    assert!(set.insert("kappa")?);
    assert!(set.insert("lambda")?);
    assert!(set.insert("mu")?);
    assert!(set.insert("nu")?);
    assert!(set.insert("xi")?);
    assert!(set.insert("omicron")?);
    assert!(set.insert("pi")?);
    assert!(set.insert("rho")?);
    assert!(set.insert("sigma")?);
    assert!(set.insert("tau")?);
    assert!(set.insert("upsilon")?);
    assert!(set.insert("phi")?);
    assert!(set.insert("chi")?);
    assert!(set.insert("psi")?);
    assert!(set.insert("omega")?);
    let mut iter = set.iter()?;
    assert_eq!(iter.size_hint(), (24, Some(24)));

    // Cbor will be ordered by size first
    assert_eq!(&iter.next_back().unwrap()?, "zeta");
    assert_eq!(iter.size_hint(), (23, Some(23)));
    assert_eq!(&iter.next().unwrap()?, "alpha");
    assert_eq!(iter.size_hint(), (22, Some(22)));
    let values: Result<Vec<String>, _> = iter.collect();
    assert_eq!(
        values?,
        vec![
            String::from("beta"),
            String::from("chi"),
            String::from("delta"),
            String::from("epsilon"),
            String::from("eta"),
            String::from("gamma"),
            String::from("iota"),
            String::from("kappa"),
            String::from("lambda"),
            String::from("mu"),
            String::from("nu"),
            String::from("omega"),
            String::from("omicron"),
            String::from("phi"),
            String::from("pi"),
            String::from("psi"),
            String::from("rho"),
            String::from("sigma"),
            String::from("tau"),
            String::from("theta"),
            String::from("upsilon"),
            String::from("xi"),
        ]
    );
    let values: Result<Vec<String>, _> = set.iter()?.rev().collect();
    assert_eq!(
        values?,
        vec![
            String::from("zeta"),
            String::from("xi"),
            String::from("upsilon"),
            String::from("theta"),
            String::from("tau"),
            String::from("sigma"),
            String::from("rho"),
            String::from("psi"),
            String::from("pi"),
            String::from("phi"),
            String::from("omicron"),
            String::from("omega"),
            String::from("nu"),
            String::from("mu"),
            String::from("lambda"),
            String::from("kappa"),
            String::from("iota"),
            String::from("gamma"),
            String::from("eta"),
            String::from("epsilon"),
            String::from("delta"),
            String::from("chi"),
            String::from("beta"),
            String::from("alpha"),
        ]
    );
    Ok(())
}

#[cfg(feature = "cbor")]
#[test]
fn test_insert_cbor() {
    let mut connection = Connection::open_in_memory().unwrap();
    let mut set: BTreeSet<Cbor<String>, _> = BTreeSet::open(&mut connection).unwrap();
    assert_eq!(set.first().unwrap(), None);
    assert_eq!(set.last().unwrap(), None);
    assert!(!set.contains("hello").unwrap());
    assert!(set.insert("hello").unwrap());
    assert_eq!(set.first().unwrap(), Some(String::from("hello")));
    assert_eq!(set.last().unwrap(), Some(String::from("hello")));
    assert!(set.contains("hello").unwrap());
    assert!(!set.insert(&String::from("hello")).unwrap());
    set.insert("foo").unwrap();
    set.insert("bar").unwrap();
    assert_eq!(set.first().unwrap(), Some(String::from("bar")));
    assert_eq!(set.last().unwrap(), Some(String::from("hello")));
}

#[cfg(feature = "cbor")]
#[test]
fn test_iter_order_cbor() -> Result<(), Box<dyn std::error::Error>> {
    let mut connection = Connection::open_in_memory()?;
    let mut set: BTreeSet<Cbor<String>, _> = BTreeSet::open(&mut connection)?;
    assert!(set.insert("alpha")?);
    assert!(set.insert("beta")?);
    assert!(set.insert("gamma")?);
    assert!(set.insert("delta")?);
    assert!(set.insert("epsilon")?);
    assert!(set.insert("zeta")?);
    assert!(set.insert("eta")?);
    assert!(set.insert("theta")?);
    assert!(set.insert("iota")?);
    assert!(set.insert("kappa")?);
    assert!(set.insert("lambda")?);
    assert!(set.insert("mu")?);
    assert!(set.insert("nu")?);
    assert!(set.insert("xi")?);
    assert!(set.insert("omicron")?);
    assert!(set.insert("pi")?);
    assert!(set.insert("rho")?);
    assert!(set.insert("sigma")?);
    assert!(set.insert("tau")?);
    assert!(set.insert("upsilon")?);
    assert!(set.insert("phi")?);
    assert!(set.insert("chi")?);
    assert!(set.insert("psi")?);
    assert!(set.insert("omega")?);
    let mut iter = set.iter()?;
    assert_eq!(iter.size_hint(), (24, Some(24)));

    // Cbor will be ordered by size first
    assert_eq!(&iter.next_back().unwrap()?, "upsilon");
    assert_eq!(iter.size_hint(), (23, Some(23)));
    assert_eq!(&iter.next().unwrap()?, "mu");
    assert_eq!(iter.size_hint(), (22, Some(22)));
    let values: Result<Vec<String>, _> = iter.collect();
    assert_eq!(
        values?,
        vec![
            String::from("nu"),
            String::from("pi"),
            String::from("xi"),
            String::from("chi"),
            String::from("eta"),
            String::from("phi"),
            String::from("psi"),
            String::from("rho"),
            String::from("tau"),
            String::from("beta"),
            String::from("iota"),
            String::from("zeta"),
            String::from("alpha"),
            String::from("delta"),
            String::from("gamma"),
            String::from("kappa"),
            String::from("omega"),
            String::from("sigma"),
            String::from("theta"),
            String::from("lambda"),
            String::from("epsilon"),
            String::from("omicron"),
        ]
    );
    let values: Result<Vec<String>, _> = set.iter()?.rev().collect();
    assert_eq!(
        values?,
        vec![
            String::from("upsilon"),
            String::from("omicron"),
            String::from("epsilon"),
            String::from("lambda"),
            String::from("theta"),
            String::from("sigma"),
            String::from("omega"),
            String::from("kappa"),
            String::from("gamma"),
            String::from("delta"),
            String::from("alpha"),
            String::from("zeta"),
            String::from("iota"),
            String::from("beta"),
            String::from("tau"),
            String::from("rho"),
            String::from("psi"),
            String::from("phi"),
            String::from("eta"),
            String::from("chi"),
            String::from("xi"),
            String::from("pi"),
            String::from("nu"),
            String::from("mu"),
        ]
    );
    Ok(())
}

#[cfg(feature = "json")]
#[test]
fn test_insert_json() {
    let mut connection = Connection::open_in_memory().unwrap();
    let mut set: BTreeSet<Json<String>, _> = BTreeSet::open(&mut connection).unwrap();
    assert_eq!(set.first().unwrap(), None);
    assert_eq!(set.last().unwrap(), None);
    assert!(!set.contains("hello").unwrap());
    assert!(set.insert("hello").unwrap());
    assert_eq!(set.first().unwrap(), Some(String::from("hello")));
    assert_eq!(set.last().unwrap(), Some(String::from("hello")));
    assert!(set.contains("hello").unwrap());
    assert!(!set.insert(&String::from("hello")).unwrap());
    set.insert("foo").unwrap();
    set.insert("bar").unwrap();
    assert_eq!(set.first().unwrap(), Some(String::from("bar")));
    assert_eq!(set.last().unwrap(), Some(String::from("hello")));
}

#[cfg(feature = "json")]
#[test]
fn test_iter_order_json() -> Result<(), Box<dyn std::error::Error>> {
    let mut connection = Connection::open_in_memory()?;
    let mut set: BTreeSet<Json<String>, _> = BTreeSet::open(&mut connection)?;
    assert!(set.insert("alpha")?);
    assert!(set.insert("beta")?);
    assert!(set.insert("gamma")?);
    assert!(set.insert("delta")?);
    assert!(set.insert("epsilon")?);
    assert!(set.insert("zeta")?);
    assert!(set.insert("eta")?);
    assert!(set.insert("theta")?);
    assert!(set.insert("iota")?);
    assert!(set.insert("kappa")?);
    assert!(set.insert("lambda")?);
    assert!(set.insert("mu")?);
    assert!(set.insert("nu")?);
    assert!(set.insert("xi")?);
    assert!(set.insert("omicron")?);
    assert!(set.insert("pi")?);
    assert!(set.insert("rho")?);
    assert!(set.insert("sigma")?);
    assert!(set.insert("tau")?);
    assert!(set.insert("upsilon")?);
    assert!(set.insert("phi")?);
    assert!(set.insert("chi")?);
    assert!(set.insert("psi")?);
    assert!(set.insert("omega")?);
    let mut iter = set.iter()?;
    assert_eq!(iter.size_hint(), (24, Some(24)));

    // Json will be ordered by size first
    assert_eq!(&iter.next_back().unwrap()?, "zeta");
    assert_eq!(iter.size_hint(), (23, Some(23)));
    assert_eq!(&iter.next().unwrap()?, "alpha");
    assert_eq!(iter.size_hint(), (22, Some(22)));
    let values: Result<Vec<String>, _> = iter.collect();
    assert_eq!(
        values?,
        vec![
            String::from("beta"),
            String::from("chi"),
            String::from("delta"),
            String::from("epsilon"),
            String::from("eta"),
            String::from("gamma"),
            String::from("iota"),
            String::from("kappa"),
            String::from("lambda"),
            String::from("mu"),
            String::from("nu"),
            String::from("omega"),
            String::from("omicron"),
            String::from("phi"),
            String::from("pi"),
            String::from("psi"),
            String::from("rho"),
            String::from("sigma"),
            String::from("tau"),
            String::from("theta"),
            String::from("upsilon"),
            String::from("xi"),
        ]
    );
    let values: Result<Vec<String>, _> = set.iter()?.rev().collect();
    assert_eq!(
        values?,
        vec![
            String::from("zeta"),
            String::from("xi"),
            String::from("upsilon"),
            String::from("theta"),
            String::from("tau"),
            String::from("sigma"),
            String::from("rho"),
            String::from("psi"),
            String::from("pi"),
            String::from("phi"),
            String::from("omicron"),
            String::from("omega"),
            String::from("nu"),
            String::from("mu"),
            String::from("lambda"),
            String::from("kappa"),
            String::from("iota"),
            String::from("gamma"),
            String::from("eta"),
            String::from("epsilon"),
            String::from("delta"),
            String::from("chi"),
            String::from("beta"),
            String::from("alpha"),
        ]
    );
    Ok(())
}

#[cfg(feature = "postcard")]
#[test]
fn test_insert_postcard() {
    let mut connection = Connection::open_in_memory().unwrap();
    let mut set: BTreeSet<Postcard<String>, _> = BTreeSet::open(&mut connection).unwrap();
    assert_eq!(set.first().unwrap(), None);
    assert_eq!(set.last().unwrap(), None);
    assert!(!set.contains("hello").unwrap());
    assert!(set.insert("hello").unwrap());
    assert_eq!(set.first().unwrap(), Some(String::from("hello")));
    assert_eq!(set.last().unwrap(), Some(String::from("hello")));
    assert!(set.contains("hello").unwrap());
    assert!(!set.insert(&String::from("hello")).unwrap());
    set.insert("foo").unwrap();
    set.insert("bar").unwrap();
    assert_eq!(set.first().unwrap(), Some(String::from("bar")));
    assert_eq!(set.last().unwrap(), Some(String::from("hello")));
}

#[cfg(feature = "postcard")]
#[test]
fn test_iter_order_postcard() -> Result<(), Box<dyn std::error::Error>> {
    let mut connection = Connection::open_in_memory()?;
    let mut set: BTreeSet<Postcard<String>, _> = BTreeSet::open(&mut connection)?;
    assert!(set.insert("alpha")?);
    assert!(set.insert("beta")?);
    assert!(set.insert("gamma")?);
    assert!(set.insert("delta")?);
    assert!(set.insert("epsilon")?);
    assert!(set.insert("zeta")?);
    assert!(set.insert("eta")?);
    assert!(set.insert("theta")?);
    assert!(set.insert("iota")?);
    assert!(set.insert("kappa")?);
    assert!(set.insert("lambda")?);
    assert!(set.insert("mu")?);
    assert!(set.insert("nu")?);
    assert!(set.insert("xi")?);
    assert!(set.insert("omicron")?);
    assert!(set.insert("pi")?);
    assert!(set.insert("rho")?);
    assert!(set.insert("sigma")?);
    assert!(set.insert("tau")?);
    assert!(set.insert("upsilon")?);
    assert!(set.insert("phi")?);
    assert!(set.insert("chi")?);
    assert!(set.insert("psi")?);
    assert!(set.insert("omega")?);
    let mut iter = set.iter()?;
    assert_eq!(iter.size_hint(), (24, Some(24)));

    assert_eq!(&iter.next_back().unwrap()?, "upsilon");
    assert_eq!(iter.size_hint(), (23, Some(23)));
    assert_eq!(&iter.next().unwrap()?, "mu");
    assert_eq!(iter.size_hint(), (22, Some(22)));
    let values: Result<Vec<String>, _> = iter.collect();
    assert_eq!(
        values?,
        vec![
            String::from("nu"),
            String::from("pi"),
            String::from("xi"),
            String::from("chi"),
            String::from("eta"),
            String::from("phi"),
            String::from("psi"),
            String::from("rho"),
            String::from("tau"),
            String::from("beta"),
            String::from("iota"),
            String::from("zeta"),
            String::from("alpha"),
            String::from("delta"),
            String::from("gamma"),
            String::from("kappa"),
            String::from("omega"),
            String::from("sigma"),
            String::from("theta"),
            String::from("lambda"),
            String::from("epsilon"),
            String::from("omicron"),
        ]
    );
    let values: Result<Vec<String>, _> = set.iter()?.rev().collect();
    assert_eq!(
        values?,
        vec![
            String::from("upsilon"),
            String::from("omicron"),
            String::from("epsilon"),
            String::from("lambda"),
            String::from("theta"),
            String::from("sigma"),
            String::from("omega"),
            String::from("kappa"),
            String::from("gamma"),
            String::from("delta"),
            String::from("alpha"),
            String::from("zeta"),
            String::from("iota"),
            String::from("beta"),
            String::from("tau"),
            String::from("rho"),
            String::from("psi"),
            String::from("phi"),
            String::from("eta"),
            String::from("chi"),
            String::from("xi"),
            String::from("pi"),
            String::from("nu"),
            String::from("mu"),
        ]
    );
    Ok(())
}

use crate::serializer::Serializer;

#[derive(Debug)]
pub enum Ipv4 {}

impl Serializer for Ipv4 {
    type Target = Ipv4Addr;

    type TargetBorrowed = Ipv4Addr;

    type BufferBorrowed = u32;

    type Buffer = u32;

    type SerializeError = Infallible;

    type DeserializeError = Infallible;

    fn sql_type() -> &'static str {
        "INTEGER"
    }

    fn serialize(target: &Self::TargetBorrowed) -> Result<Self::Buffer, Self::SerializeError> {
        Ok((*target).into())
    }

    fn deserialize(data: &Self::BufferBorrowed) -> Result<Self::Target, Self::DeserializeError> {
        Ok((*data).into())
    }
}

#[test]
fn test_insert_ipv4() -> Result<(), Box<dyn std::error::Error>> {
    let connection = Connection::open_in_memory()?;
    let mut set: BTreeSet<Ipv4, _> = BTreeSet::open(connection)?;
    assert_eq!(set.first()?, None);
    assert_eq!(set.last()?, None);
    assert!(!set.contains(&Ipv4Addr::new(127, 0, 0, 1))?);
    assert!(set.insert(&Ipv4Addr::new(127, 0, 0, 1))?);
    assert_eq!(set.first()?, Some(Ipv4Addr::new(127, 0, 0, 1)));
    assert_eq!(set.last()?, Some(Ipv4Addr::new(127, 0, 0, 1)));
    assert!(set.contains(&Ipv4Addr::new(127, 0, 0, 1))?);
    assert!(!set.insert(&Ipv4Addr::new(127, 0, 0, 1))?);
    set.insert(&Ipv4Addr::new(127, 0, 0, 2))?;
    set.insert(&Ipv4Addr::new(126, 0, 0, 2))?;
    assert_eq!(set.first()?, Some(Ipv4Addr::new(126, 0, 0, 2)));
    assert_eq!(set.last()?, Some(Ipv4Addr::new(127, 0, 0, 2)));
    Ok(())
}
