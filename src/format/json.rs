use std::{marker::PhantomData, ops::Deref};

use serde::{de::DeserializeOwned, Serialize};

use super::Format;

/// A Json format.
///
/// This is probably the best format available for interoperability.
pub struct Json<Out, In = <Out as Deref>::Target>(PhantomData<Out>, PhantomData<In>)
where
    In: Serialize + ?Sized,
    Out: DeserializeOwned;

impl<Out, In> Format for Json<Out, In>
where
    In: Serialize + ?Sized,
    Out: DeserializeOwned,
{
    type Out = Out;
    type In = In;
    type Buffer = String;
    type SerializeError = serde_json::Error;
    type DeserializeError = serde_json::Error;

    fn sql_type() -> &'static str {
        "TEXT"
    }

    fn serialize(target: &Self::In) -> Result<Self::Buffer, Self::SerializeError> {
        serde_json::to_string(target)
    }

    fn deserialize(data: &Self::Buffer) -> Result<Self::Out, Self::DeserializeError> {
        serde_json::from_str(data)
    }
}
