use std::{marker::PhantomData, ops::Deref};

use serde::{de::DeserializeOwned, Serialize};

use super::Format;

/// A msgpack serde format.
pub struct Msgpack<Out, In = <Out as Deref>::Target>(PhantomData<Out>, PhantomData<In>)
where
    In: Serialize + ?Sized,
    Out: DeserializeOwned;

impl<Out, In> Format for Msgpack<Out, In>
where
    In: Serialize + ?Sized,
    Out: DeserializeOwned,
{
    type Out = Out;
    type In = In;
    type Buffer = Vec<u8>;
    type SerializeError = rmp_serde::encode::Error;
    type DeserializeError = rmp_serde::decode::Error;

    fn sql_type() -> &'static str {
        "BLOB"
    }

    fn serialize(target: &Self::In) -> Result<Self::Buffer, Self::SerializeError> {
        rmp_serde::to_vec(target)
    }

    fn deserialize(data: &Self::Buffer) -> Result<Self::Out, Self::DeserializeError> {
        rmp_serde::from_slice(data)
    }
}
