use std::{marker::PhantomData, ops::Deref};

use serde::{de::DeserializeOwned, Serialize};

use super::Format;

/// A postcard serde format.
///
/// This is a good choice for structured formats that have binary needs and
/// won't need to talk to other programming languages.
pub struct Postcard<Out, In = <Out as Deref>::Target>(PhantomData<Out>, PhantomData<In>)
where
    In: Serialize + ?Sized,
    Out: DeserializeOwned;

impl<Out, In> Format for Postcard<Out, In>
where
    In: Serialize + ?Sized,
    Out: DeserializeOwned,
{
    type Out = Out;
    type In = In;
    type Buffer = Vec<u8>;
    type SerializeError = postcard::Error;
    type DeserializeError = postcard::Error;

    fn sql_type() -> &'static str {
        "BLOB"
    }

    fn serialize(target: &Self::In) -> Result<Self::Buffer, Self::SerializeError> {
        postcard::to_stdvec(target)
    }

    fn deserialize(data: &Self::Buffer) -> Result<Self::Out, Self::DeserializeError> {
        postcard::from_bytes(data)
    }
}
