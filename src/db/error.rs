use std::fmt;

#[derive(Debug)]
pub enum SetupError {
    Sqlite(rusqlite::Error),
    ApplicationId(i32),
    UserVersion(i32),
    TableType { expected: String, actual: String },
}

impl From<rusqlite::Error> for SetupError {
    fn from(v: rusqlite::Error) -> Self {
        Self::Sqlite(v)
    }
}

impl fmt::Display for SetupError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SetupError::Sqlite(e) => write!(f, "sqlite error: {e}"),
            SetupError::ApplicationId(id) => write!(f, "wrong application id: {id}"),
            SetupError::UserVersion(version) => write!(f, "illegal user version: {version}"),
            SetupError::TableType { expected, actual } => {
                write!(
                    f,
                    "table was wrong type: expected={expected} actual={actual}"
                )
            }
        }
    }
}

impl std::error::Error for SetupError {}

#[derive(Debug)]
pub enum SetVersionError {
    Sqlite(rusqlite::Error),
}

impl From<rusqlite::Error> for SetVersionError {
    fn from(v: rusqlite::Error) -> Self {
        Self::Sqlite(v)
    }
}

impl fmt::Display for SetVersionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SetVersionError::Sqlite(e) => write!(f, "sqlite error: {e}"),
        }
    }
}

impl std::error::Error for SetVersionError {}
