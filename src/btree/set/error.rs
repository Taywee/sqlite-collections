use crate::db::{SetVersionError, SetupError};
use crate::serializer::Serializer;
use std::fmt;

#[derive(Debug)]
pub enum OpenError {
    Sqlite(rusqlite::Error),
    ApplicationId(i32),
    TableVersion(i64),
    UserVersion(i32),
    TableType { expected: String, actual: String },
}

impl From<rusqlite::Error> for OpenError {
    fn from(v: rusqlite::Error) -> Self {
        Self::Sqlite(v)
    }
}

impl From<SetupError> for OpenError {
    fn from(v: SetupError) -> Self {
        match v {
            SetupError::Sqlite(e) => OpenError::Sqlite(e),
            SetupError::ApplicationId(e) => OpenError::ApplicationId(e),
            SetupError::UserVersion(e) => OpenError::UserVersion(e),
            SetupError::TableType { expected, actual } => OpenError::TableType { expected, actual },
        }
    }
}
impl From<SetVersionError> for OpenError {
    fn from(v: SetVersionError) -> Self {
        match v {
            SetVersionError::Sqlite(e) => OpenError::Sqlite(e),
        }
    }
}

impl fmt::Display for OpenError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            OpenError::Sqlite(e) => write!(f, "sqlite error: {e}"),
            OpenError::ApplicationId(id) => write!(f, "wrong application id: {id}"),
            OpenError::UserVersion(version) => write!(f, "illegal user version: {version}"),
            OpenError::TableVersion(version) => write!(f, "illegal table version: {version}"),
            OpenError::TableType { expected, actual } => {
                write!(
                    f,
                    "table was wrong type: expected={expected} actual={actual}"
                )
            }
        }
    }
}

impl std::error::Error for OpenError {}

#[derive(Debug)]
pub enum Error<S>
where
    S: Serializer,
{
    Sqlite(rusqlite::Error),
    Serialize(S::SerializeError),
    Deserialize(S::DeserializeError),
}

impl<S> From<rusqlite::Error> for Error<S>
where
    S: Serializer,
{
    fn from(v: rusqlite::Error) -> Self {
        Self::Sqlite(v)
    }
}

impl<S> fmt::Display for Error<S>
where
    S: Serializer,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Sqlite(e) => write!(f, "sqlite error: {e}"),
            Error::Serialize(e) => write!(f, "serialize error: {e}"),
            Error::Deserialize(e) => write!(f, "deserialize error: {e}"),
        }
    }
}

impl<S> std::error::Error for Error<S> where S: Serializer {}
