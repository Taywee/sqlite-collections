use rusqlite::{Connection, Savepoint, Transaction};

/// Simple trait to work around:
/// * AsRef isn't implemented for T, so Connection couldn't be owned.
/// * Deref doesn't work automatically for references, so &Savepoint won't work
/// * Borrow would work, but it's not implemented for Savepoint or Transaction
/// This is similar to Savepointable, but works immutably and doesn't care about
/// making savepoints.
pub trait AsConnection {
    fn as_connection(&self) -> &Connection;
}

impl AsConnection for Connection {
    fn as_connection(&self) -> &Connection {
        self
    }
}

impl AsConnection for &Connection {
    fn as_connection(&self) -> &Connection {
        *self
    }
}

impl AsConnection for Transaction<'_> {
    fn as_connection(&self) -> &Connection {
        &*self
    }
}
impl AsConnection for &Transaction<'_> {
    fn as_connection(&self) -> &Connection {
        &*self
    }
}

impl AsConnection for Savepoint<'_> {
    fn as_connection(&self) -> &Connection {
        &*self
    }
}
impl AsConnection for &Savepoint<'_> {
    fn as_connection(&self) -> &Connection {
        &*self
    }
}
