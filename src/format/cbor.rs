use std::{io, marker::PhantomData, ops::Deref};

use serde::{de::DeserializeOwned, Serialize};

use super::Format;

/// A CBOR serde format.
///
/// CBOR is similar to msgpack.
pub struct Cbor<Out, In = <Out as Deref>::Target>(PhantomData<Out>, PhantomData<In>)
where
    In: Serialize + ?Sized,
    Out: DeserializeOwned;

impl<Out, In> Format for Cbor<Out, In>
where
    In: Serialize + ?Sized,
    Out: DeserializeOwned,
{
    type Out = Out;
    type In = In;
    type Buffer = Vec<u8>;
    type SerializeError = ciborium::ser::Error<io::Error>;
    type DeserializeError = ciborium::de::Error<io::Error>;

    fn sql_type() -> &'static str {
        "BLOB"
    }

    fn serialize(target: &Self::In) -> Result<Self::Buffer, Self::SerializeError> {
        let mut buffer: Vec<u8> = Vec::with_capacity(4096);
        ciborium::into_writer(target, &mut buffer)?;
        Ok(buffer)
    }

    fn deserialize(data: &Self::Buffer) -> Result<Self::Out, Self::DeserializeError> {
        ciborium::from_reader(data.as_slice())
    }
}
