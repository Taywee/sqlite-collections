use crate::format::Format;
use std::fmt;

pub enum Error<S>
where
    S: Format,
{
    Sqlite(rusqlite::Error),
    Fmt(std::fmt::Error),
    Serialize(S::SerializeError),
    Deserialize(S::DeserializeError),
}

impl<S> fmt::Debug for Error<S>
where
    S: Format,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Sqlite(e) => write!(f, "Error::Sqlite({e:?})"),
            Error::Fmt(e) => write!(f, "Error::Fmt({e:?})"),
            Error::Serialize(e) => write!(f, "Error::Serialize({e:?})"),
            Error::Deserialize(e) => write!(f, "Error::Deserialize({e:?})"),
        }
    }
}

impl<S> From<std::fmt::Error> for Error<S>
where
    S: Format,
{
    fn from(v: std::fmt::Error) -> Self {
        Self::Fmt(v)
    }
}

impl<S> From<rusqlite::Error> for Error<S>
where
    S: Format,
{
    fn from(v: rusqlite::Error) -> Self {
        Self::Sqlite(v)
    }
}

impl<S> fmt::Display for Error<S>
where
    S: Format,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Sqlite(e) => write!(f, "sqlite error: {e}"),
            Error::Fmt(e) => write!(f, "fmt error: {e}"),
            Error::Serialize(e) => write!(f, "serialize error: {e}"),
            Error::Deserialize(e) => write!(f, "deserialize error: {e}"),
        }
    }
}

impl<S> std::error::Error for Error<S> where S: Format {}
