use std::{borrow::Cow, convert::Infallible, net::Ipv4Addr};

use crate::format::{Format, Parse};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "serde")]
#[derive(Serialize, Deserialize, PartialEq, PartialOrd, Ord, Eq, Debug)]
pub struct User<'a> {
    pub id: u64,
    pub name: Cow<'a, str>,
}

#[cfg(feature = "serde")]
#[derive(Serialize, Deserialize, PartialEq, PartialOrd, Ord, Eq, Debug)]
pub struct Repository<'a> {
    pub id: u64,
    pub owner: User<'a>,
}

use super::*;
use crate::format::Raw;

#[cfg(feature = "cbor")]
use crate::format::Cbor;

#[cfg(feature = "json")]
use crate::format::Json;

#[cfg(feature = "postcard")]
use crate::format::Postcard;

#[cfg(feature = "msgpack")]
use crate::format::Msgpack;

macro_rules! insert_test {
    ($format:ident) => {{
        let connection = Connection::open_in_memory().unwrap();
        let mut set: Set<$format<String>, _> = Set::open(connection).unwrap();
        assert_eq!(set.first().unwrap(), None);
        assert_eq!(set.last().unwrap(), None);
        assert!(set.is_empty().unwrap());
        assert!(!set.contains("hello").unwrap());
        assert!(set.insert("hello").unwrap());
        assert_eq!(set.first().unwrap(), Some(String::from("hello")));
        assert_eq!(set.last().unwrap(), Some(String::from("hello")));
        assert!(set.contains("hello").unwrap());
        assert!(!set.is_empty().unwrap());
        assert!(!set.insert(&String::from("hello")).unwrap());
        set.insert("foo").unwrap();
        set.insert("bar").unwrap();
        assert_eq!(set.first().unwrap(), Some(String::from("bar")));
        assert_eq!(set.last().unwrap(), Some(String::from("hello")));
    }};
}

macro_rules! serde_insert_test {
    ($format:ident) => {{
        use $crate::ds::set::test::{Repository, User};
        let connection = Connection::open_in_memory().unwrap();
        let mut set: Set<$format<Repository, Repository<'static>>, _> =
            Set::open(connection).unwrap();
        let taylor = Repository {
            id: 500,
            owner: User {
                id: 600,
                name: Cow::Borrowed("taylor"),
            },
        };
        let amber = Repository {
            id: 501,
            owner: User {
                id: 601,
                name: Cow::Borrowed("amber"),
            },
        };

        assert!(!set.contains(&taylor).unwrap());
        assert!(!set.contains(&amber).unwrap());
        set.insert(&taylor).unwrap();
        assert!(set.contains(&taylor).unwrap());
        assert!(!set.contains(&amber).unwrap());
        assert_eq!(set.first().unwrap(), Some(taylor));
    }};
}

macro_rules! order_test_lexicographic {
    ($format:ident) => {{
        let mut connection = Connection::open_in_memory().unwrap();
        let mut set: Set<$format<String>, _> = Set::open(&mut connection).unwrap();
        assert!(set.insert("alpha").unwrap());
        assert!(set.insert("beta").unwrap());
        assert!(set.insert("gamma").unwrap());
        assert!(set.insert("delta").unwrap());
        assert!(set.insert("epsilon").unwrap());
        assert!(set.insert("zeta").unwrap());
        assert!(set.insert("eta").unwrap());
        assert!(set.insert("theta").unwrap());
        assert!(set.insert("iota").unwrap());
        assert!(set.insert("kappa").unwrap());
        assert!(set.insert("lambda").unwrap());
        assert!(set.insert("mu").unwrap());
        assert!(set.insert("nu").unwrap());
        assert!(set.insert("xi").unwrap());
        assert!(set.insert("omicron").unwrap());
        assert!(set.insert("pi").unwrap());
        assert!(set.insert("rho").unwrap());
        assert!(set.insert("sigma").unwrap());
        assert!(set.insert("tau").unwrap());
        assert!(set.insert("upsilon").unwrap());
        assert!(set.insert("phi").unwrap());
        assert!(set.insert("chi").unwrap());
        assert!(set.insert("psi").unwrap());
        assert!(set.insert("omega").unwrap());
        let mut iter = set.iter().unwrap();
        assert_eq!(iter.size_hint(), (24, Some(24)));

        assert_eq!(&iter.next_back().unwrap().unwrap(), "zeta");
        assert_eq!(iter.size_hint(), (23, Some(23)));
        assert_eq!(&iter.next().unwrap().unwrap(), "alpha");
        assert_eq!(iter.size_hint(), (22, Some(22)));
        let values: Result<Vec<String>, _> = iter.collect();
        assert_eq!(
            values.unwrap(),
            vec![
                String::from("beta"),
                String::from("chi"),
                String::from("delta"),
                String::from("epsilon"),
                String::from("eta"),
                String::from("gamma"),
                String::from("iota"),
                String::from("kappa"),
                String::from("lambda"),
                String::from("mu"),
                String::from("nu"),
                String::from("omega"),
                String::from("omicron"),
                String::from("phi"),
                String::from("pi"),
                String::from("psi"),
                String::from("rho"),
                String::from("sigma"),
                String::from("tau"),
                String::from("theta"),
                String::from("upsilon"),
                String::from("xi"),
            ]
        );
        let values: Result<Vec<String>, _> = set.iter().unwrap().rev().collect();
        assert_eq!(
            values.unwrap(),
            vec![
                String::from("zeta"),
                String::from("xi"),
                String::from("upsilon"),
                String::from("theta"),
                String::from("tau"),
                String::from("sigma"),
                String::from("rho"),
                String::from("psi"),
                String::from("pi"),
                String::from("phi"),
                String::from("omicron"),
                String::from("omega"),
                String::from("nu"),
                String::from("mu"),
                String::from("lambda"),
                String::from("kappa"),
                String::from("iota"),
                String::from("gamma"),
                String::from("eta"),
                String::from("epsilon"),
                String::from("delta"),
                String::from("chi"),
                String::from("beta"),
                String::from("alpha"),
            ]
        );
    }};
}

macro_rules! order_test_length_prefix {
    ($format:ident) => {{
        let mut connection = Connection::open_in_memory().unwrap();
        let mut set: Set<$format<String>, _> = Set::open(&mut connection).unwrap();
        assert!(set.insert("alpha").unwrap());
        assert!(set.insert("beta").unwrap());
        assert!(set.insert("gamma").unwrap());
        assert!(set.insert("delta").unwrap());
        assert!(set.insert("epsilon").unwrap());
        assert!(set.insert("zeta").unwrap());
        assert!(set.insert("eta").unwrap());
        assert!(set.insert("theta").unwrap());
        assert!(set.insert("iota").unwrap());
        assert!(set.insert("kappa").unwrap());
        assert!(set.insert("lambda").unwrap());
        assert!(set.insert("mu").unwrap());
        assert!(set.insert("nu").unwrap());
        assert!(set.insert("xi").unwrap());
        assert!(set.insert("omicron").unwrap());
        assert!(set.insert("pi").unwrap());
        assert!(set.insert("rho").unwrap());
        assert!(set.insert("sigma").unwrap());
        assert!(set.insert("tau").unwrap());
        assert!(set.insert("upsilon").unwrap());
        assert!(set.insert("phi").unwrap());
        assert!(set.insert("chi").unwrap());
        assert!(set.insert("psi").unwrap());
        assert!(set.insert("omega").unwrap());
        let mut iter = set.iter().unwrap();
        assert_eq!(iter.size_hint(), (24, Some(24)));

        assert_eq!(&iter.next_back().unwrap().unwrap(), "upsilon");
        assert_eq!(iter.size_hint(), (23, Some(23)));
        assert_eq!(&iter.next().unwrap().unwrap(), "mu");
        assert_eq!(iter.size_hint(), (22, Some(22)));
        let values: Result<Vec<String>, _> = iter.collect();
        assert_eq!(
            values.unwrap(),
            vec![
                String::from("nu"),
                String::from("pi"),
                String::from("xi"),
                String::from("chi"),
                String::from("eta"),
                String::from("phi"),
                String::from("psi"),
                String::from("rho"),
                String::from("tau"),
                String::from("beta"),
                String::from("iota"),
                String::from("zeta"),
                String::from("alpha"),
                String::from("delta"),
                String::from("gamma"),
                String::from("kappa"),
                String::from("omega"),
                String::from("sigma"),
                String::from("theta"),
                String::from("lambda"),
                String::from("epsilon"),
                String::from("omicron"),
            ]
        );
        let values: Result<Vec<String>, _> = set.iter().unwrap().rev().collect();
        assert_eq!(
            values.unwrap(),
            vec![
                String::from("upsilon"),
                String::from("omicron"),
                String::from("epsilon"),
                String::from("lambda"),
                String::from("theta"),
                String::from("sigma"),
                String::from("omega"),
                String::from("kappa"),
                String::from("gamma"),
                String::from("delta"),
                String::from("alpha"),
                String::from("zeta"),
                String::from("iota"),
                String::from("beta"),
                String::from("tau"),
                String::from("rho"),
                String::from("psi"),
                String::from("phi"),
                String::from("eta"),
                String::from("chi"),
                String::from("xi"),
                String::from("pi"),
                String::from("nu"),
                String::from("mu"),
            ]
        );
    }};
}

#[test]
fn test_insert_direct() {
    insert_test!(Raw);
}

#[test]
fn test_iter_order_direct() {
    order_test_lexicographic!(Raw);
}

#[cfg(feature = "cbor")]
#[test]
fn test_insert_cbor() {
    insert_test!(Cbor);
    serde_insert_test!(Cbor);
}

#[cfg(feature = "cbor")]
#[test]
fn test_iter_order_cbor() {
    order_test_length_prefix!(Cbor);
}

#[cfg(feature = "msgpack")]
#[test]
fn test_insert_msgpack() {
    insert_test!(Msgpack);
    serde_insert_test!(Msgpack);
}

#[cfg(feature = "msgpack")]
#[test]
fn test_iter_order_msgpack() {
    order_test_length_prefix!(Msgpack);
}

#[cfg(feature = "json")]
#[test]
fn test_insert_json() {
    insert_test!(Json);
    serde_insert_test!(Json);
}

#[cfg(feature = "json")]
#[test]
fn test_iter_order_json() {
    order_test_lexicographic!(Json);
}

#[cfg(feature = "postcard")]
#[test]
fn test_insert_postcard() {
    insert_test!(Postcard);
    serde_insert_test!(Postcard);
}

#[cfg(feature = "postcard")]
#[test]
fn test_iter_order_postcard() {
    order_test_length_prefix!(Postcard);
}

#[derive(Debug)]
pub enum Ipv4 {}

impl Format for Ipv4 {
    type Out = Ipv4Addr;

    type In = Ipv4Addr;

    type Buffer = u32;

    type SerializeError = Infallible;

    type DeserializeError = Infallible;

    fn sql_type() -> &'static str {
        "INTEGER"
    }

    fn serialize(target: &Self::In) -> Result<Self::Buffer, Self::SerializeError> {
        Ok((*target).into())
    }

    fn deserialize(data: &Self::Buffer) -> Result<Self::Out, Self::DeserializeError> {
        Ok((*data).into())
    }
}

#[test]
fn test_insert_ipv4() -> Result<(), Box<dyn std::error::Error>> {
    let connection = Connection::open_in_memory()?;
    let mut set: Set<Ipv4, _> = Set::open(connection)?;
    assert_eq!(set.first()?, None);
    assert_eq!(set.last()?, None);
    assert!(!set.contains(&Ipv4Addr::new(127, 0, 0, 1))?);
    assert!(set.insert(&Ipv4Addr::new(127, 0, 0, 1))?);
    assert_eq!(set.first()?, Some(Ipv4Addr::new(127, 0, 0, 1)));
    assert_eq!(set.last()?, Some(Ipv4Addr::new(127, 0, 0, 1)));
    assert!(set.contains(&Ipv4Addr::new(127, 0, 0, 1))?);
    assert!(!set.insert(&Ipv4Addr::new(127, 0, 0, 1))?);
    set.insert(&Ipv4Addr::new(127, 0, 0, 2))?;
    set.insert(&Ipv4Addr::new(126, 0, 0, 2))?;
    assert_eq!(set.first()?, Some(Ipv4Addr::new(126, 0, 0, 2)));
    assert_eq!(set.last()?, Some(Ipv4Addr::new(127, 0, 0, 2)));
    Ok(())
}

#[test]
fn test_insert_ipv4_parse() -> Result<(), Box<dyn std::error::Error>> {
    let connection = Connection::open_in_memory()?;
    let mut set: Set<Parse<Ipv4Addr>, _> = Set::open(connection)?;
    assert_eq!(set.first()?, None);
    assert_eq!(set.last()?, None);
    assert!(!set.contains(&Ipv4Addr::new(127, 0, 0, 1))?);
    assert!(set.insert(&Ipv4Addr::new(127, 0, 0, 1))?);
    assert_eq!(set.first()?, Some(Ipv4Addr::new(127, 0, 0, 1)));
    assert_eq!(set.last()?, Some(Ipv4Addr::new(127, 0, 0, 1)));
    assert!(set.contains(&Ipv4Addr::new(127, 0, 0, 1))?);
    assert!(!set.insert(&Ipv4Addr::new(127, 0, 0, 1))?);
    set.insert(&Ipv4Addr::new(127, 0, 0, 2))?;
    set.insert(&Ipv4Addr::new(126, 0, 0, 2))?;
    assert_eq!(set.first()?, Some(Ipv4Addr::new(126, 0, 0, 2)));
    assert_eq!(set.last()?, Some(Ipv4Addr::new(127, 0, 0, 2)));
    Ok(())
}

#[test]
fn test_insert_u32() -> Result<(), Box<dyn std::error::Error>> {
    let connection = Connection::open_in_memory()?;
    let mut set: Set<Raw<u32, u32>, _> = Set::open(connection)?;
    assert_eq!(set.first()?, None);
    assert_eq!(set.last()?, None);
    assert!(!set.contains(&15)?);
    assert!(set.insert(&15)?);
    assert_eq!(set.first()?, Some(15));
    assert_eq!(set.last()?, Some(15));
    assert!(set.contains(&15)?);
    Ok(())
}
