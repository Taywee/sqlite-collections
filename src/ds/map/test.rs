use std::borrow::Cow;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "serde")]
#[derive(Serialize, Deserialize, PartialEq, PartialOrd, Ord, Eq, Debug)]
pub struct User<'a> {
    pub id: u64,
    pub name: Cow<'a, str>,
}

#[cfg(feature = "serde")]
#[derive(Serialize, Deserialize, PartialEq, PartialOrd, Ord, Eq, Debug)]
pub struct Repository<'a> {
    pub id: u64,
    pub owner: User<'a>,
}

use super::*;
use crate::format::Raw;

#[cfg(feature = "cbor")]
use crate::format::Cbor;

#[cfg(feature = "json")]
use crate::format::Json;

#[cfg(feature = "postcard")]
use crate::format::Postcard;

#[cfg(feature = "msgpack")]
use crate::format::Msgpack;

macro_rules! insert_test {
    ($format:ident) => {{
        let connection = Connection::open_in_memory().unwrap();
        let mut map: Map<$format<String>, $format<String>, _> = Map::open(connection).unwrap();
        assert!(!map.contains_key("hello").unwrap());
        assert_eq!(map.get("hello").unwrap(), None);
        assert_eq!(map.insert("hello", "world").unwrap(), None);
        assert_eq!(map.get("hello").unwrap(), Some(String::from("world")));
        assert!(map.contains_key("hello").unwrap());
        assert_eq!(
            map.insert("hello", "there").unwrap(),
            Some(String::from("world"))
        );
        assert_eq!(map.get("hello").unwrap(), Some(String::from("there")));
        //assert_eq!(map.remove("hello").unwrap(), Some(String::from("there")));
        // assert_eq!(map.get("hello").unwrap(), None);
    }};
}

macro_rules! serde_insert_test {
    ($format:ident) => {{
        use $crate::ds::map::test::{Repository, User};
        let connection = Connection::open_in_memory().unwrap();
        let mut map: Map<$format<String>, $format<Repository, Repository<'static>>, _> =
            Map::open(connection).unwrap();
        let taylor = Repository {
            id: 500,
            owner: User {
                id: 600,
                name: Cow::Borrowed("taylor"),
            },
        };
        let _amber = Repository {
            id: 501,
            owner: User {
                id: 601,
                name: Cow::Borrowed("amber"),
            },
        };

        assert!(!map.contains_key("taylor").unwrap());
        assert!(!map.contains_key("amber").unwrap());
        map.insert("taylor", &taylor).unwrap();
        assert!(map.contains_key("taylor").unwrap());
        assert!(!map.contains_key("amber").unwrap());
        // assert_eq!(map.first().unwrap(), Some(amber));
    }};
}

#[test]
fn test_insert_direct() {
    insert_test!(Raw);
}

#[cfg(feature = "cbor")]
#[test]
fn test_insert_cbor() {
    insert_test!(Cbor);
    serde_insert_test!(Cbor);
}

#[cfg(feature = "msgpack")]
#[test]
fn test_insert_msgpack() {
    insert_test!(Msgpack);
    serde_insert_test!(Msgpack);
}

#[cfg(feature = "json")]
#[test]
fn test_insert_json() {
    insert_test!(Json);
    serde_insert_test!(Json);
}

#[cfg(feature = "postcard")]
#[test]
fn test_insert_postcard() {
    insert_test!(Postcard);
    serde_insert_test!(Postcard);
}

// use crate::format::Format;

// #[derive(Debug)]
// pub enum Ipv4 {}

// impl Format for Ipv4 {
//     type Target = Ipv4Addr;

//     type TargetBorrowed = Ipv4Addr;

//     type BufferBorrowed = u32;

//     type Buffer = u32;

//     type SerializeError = Infallible;

//     type DeserializeError = Infallible;

//     fn sql_type() -> &'static str {
//         "INTEGER"
//     }

//     fn serialize(target: &Self::TargetBorrowed) -> Result<Self::Buffer, Self::SerializeError> {
//         Ok((*target).into())
//     }

//     fn deserialize(data: &Self::BufferBorrowed) -> Result<Self::Target, Self::DeserializeError> {
//         Ok((*data).into())
//     }
// }

// #[test]
// fn test_insert_ipv4() -> Result<(), Box<dyn std::error::Error>> {
//     let connection = Connection::open_in_memory()?;
//     let mut map: Map<Ipv4, _> = Map::open(connection)?;
//     assert_eq!(map.first()?, None);
//     assert_eq!(map.last()?, None);
//     assert!(!map.contains(&Ipv4Addr::new(127, 0, 0, 1))?);
//     assert!(map.insert(&Ipv4Addr::new(127, 0, 0, 1))?);
//     assert_eq!(map.first()?, Some(Ipv4Addr::new(127, 0, 0, 1)));
//     assert_eq!(map.last()?, Some(Ipv4Addr::new(127, 0, 0, 1)));
//     assert!(map.contains(&Ipv4Addr::new(127, 0, 0, 1))?);
//     assert!(!map.insert(&Ipv4Addr::new(127, 0, 0, 1))?);
//     map.insert(&Ipv4Addr::new(127, 0, 0, 2))?;
//     map.insert(&Ipv4Addr::new(126, 0, 0, 2))?;
//     assert_eq!(map.first()?, Some(Ipv4Addr::new(126, 0, 0, 2)));
//     assert_eq!(map.last()?, Some(Ipv4Addr::new(127, 0, 0, 2)));
//     Ok(())
// }
