use rusqlite::{Connection, Result, Savepoint, Transaction};

/// Types that can make a savepoint.
/// This is necessary because savepoint methods take a &mut, but none of the
// types impl DerefMut for Connection.
pub trait Savepointable {
    fn savepoint(&mut self) -> Result<Savepoint<'_>>;
}

impl Savepointable for Connection {
    #[inline]
    fn savepoint(&mut self) -> Result<Savepoint<'_>> {
        Connection::savepoint(self)
    }
}

impl Savepointable for Transaction<'_> {
    #[inline]
    fn savepoint(&mut self) -> Result<Savepoint<'_>> {
        Transaction::savepoint(self)
    }
}

impl Savepointable for Savepoint<'_> {
    #[inline]
    fn savepoint(&mut self) -> Result<Savepoint<'_>> {
        Savepoint::savepoint(self)
    }
}

impl Savepointable for &mut Connection {
    #[inline]
    fn savepoint(&mut self) -> Result<Savepoint<'_>> {
        Connection::savepoint(*self)
    }
}

impl Savepointable for &mut Transaction<'_> {
    #[inline]
    fn savepoint(&mut self) -> Result<Savepoint<'_>> {
        Transaction::savepoint(*self)
    }
}

impl Savepointable for &mut Savepoint<'_> {
    #[inline]
    fn savepoint(&mut self) -> Result<Savepoint<'_>> {
        Savepoint::savepoint(*self)
    }
}
