use rusqlite::{types::FromSql, ToSql};
use std::{borrow::Borrow, convert::Infallible, marker::PhantomData, ops::Deref};

use crate::db;

use super::Format;

/// A plain SQL format, just serializing and deserializing raw sql types
/// directly.
///
/// This is the easiest way of working with this library, and is a great choice
/// for keys.
pub struct Raw<Out, In = <Out as Deref>::Target>(PhantomData<Out>, PhantomData<In>)
where
    In: ToSql + ?Sized + ToOwned<Owned = Out>,
    Out: ToSql + FromSql + Clone,
    Out: Borrow<In>;

impl<Out, In> Format for Raw<Out, In>
where
    In: ToSql + ?Sized + ToOwned<Owned = Out>,
    Out: ToSql + FromSql + Clone,
    Out: Borrow<In>,
{
    type Out = Out;
    type In = In;
    type Buffer = Out;
    type SerializeError = Infallible;
    type DeserializeError = Infallible;

    fn sql_type() -> &'static str {
        db::any()
    }

    fn serialize(object: &Self::In) -> Result<Self::Buffer, Self::SerializeError> {
        Ok(object.to_owned())
    }

    fn deserialize(data: &Self::Buffer) -> Result<Self::Out, Self::DeserializeError> {
        Ok(data.clone())
    }
}
