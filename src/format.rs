use rusqlite::{types::FromSql, ToSql};

/// The trait that tells collections how to serialize and deserialize their
/// types.
/// Serializers are typically built on Serde, but aren't required to be.  For
/// instance, `Ipv4Addr` can be directly stored and loaded as a SQLite INTEGER
/// using the `From` Trait, and [`Parse`] can be used for types that implement
/// Display and FromStr.
///
/// [`Parse`]: struct.Parse.html
pub trait Format {
    /// The type that is passed into function calls, for insertion, checking, etc.
    /// This type is serialized.
    type In: ?Sized;

    /// The type that is returned from function calls.
    /// This type is deserialized.
    type Out;

    /// An owned buffer, for serialization and deserialization.  This is not
    /// exposed to the user.
    type Buffer: ToSql + FromSql;

    type SerializeError: std::error::Error;
    type DeserializeError: std::error::Error;

    /// The column type.  This is not a constant, because you might want to
    /// query the SQLite version to select the best type for the version (for
    /// instance, selecting ANY for a STRICT table only where it's available,
    /// and BLOB otherwise).
    fn sql_type() -> &'static str;

    /// Serialize a borrowed target into a buffer.
    fn serialize(target: &Self::In) -> Result<Self::Buffer, Self::SerializeError>;

    /// Deserialize a target from a borrowed buffer.
    fn deserialize(data: &Self::Buffer) -> Result<Self::Out, Self::DeserializeError>;
}

mod raw;
pub use raw::Raw;

mod parse;
pub use parse::Parse;

#[cfg(feature = "cbor")]
mod cbor;

#[cfg(feature = "cbor")]
pub use cbor::Cbor;

#[cfg(feature = "json")]
mod json;

#[cfg(feature = "json")]
pub use json::Json;

#[cfg(feature = "postcard")]
mod postcard;

#[cfg(feature = "postcard")]
pub use self::postcard::Postcard;

#[cfg(feature = "msgpack")]
mod msgpack;

#[cfg(feature = "msgpack")]
pub use self::msgpack::Msgpack;
