use crate::format::Format;
use std::fmt;

pub enum Error<K, V>
where
    K: Format,
    V: Format,
{
    Sqlite(rusqlite::Error),
    Fmt(std::fmt::Error),
    KeySerialize(K::SerializeError),
    KeyDeserialize(K::DeserializeError),
    ValueSerialize(V::SerializeError),
    ValueDeserialize(V::DeserializeError),
}

impl<K, V> fmt::Debug for Error<K, V>
where
    K: Format,
    V: Format,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Sqlite(e) => write!(f, "Error::Sqlite({e:?})"),
            Error::Fmt(e) => write!(f, "Error::Fmt({e:?})"),
            Error::KeySerialize(e) => write!(f, "Error::KeySerialize({e:?})"),
            Error::KeyDeserialize(e) => write!(f, "Error::KeyDeserialize({e:?})"),
            Error::ValueSerialize(e) => write!(f, "Error::ValueSerialize({e:?})"),
            Error::ValueDeserialize(e) => write!(f, "Error::ValueDeserialize({e:?})"),
        }
    }
}

impl<K, V> From<std::fmt::Error> for Error<K, V>
where
    K: Format,
    V: Format,
{
    fn from(v: std::fmt::Error) -> Self {
        Self::Fmt(v)
    }
}

impl<K, V> From<rusqlite::Error> for Error<K, V>
where
    K: Format,
    V: Format,
{
    fn from(v: rusqlite::Error) -> Self {
        Self::Sqlite(v)
    }
}

impl<K, V> fmt::Display for Error<K, V>
where
    K: Format,
    V: Format,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Sqlite(e) => write!(f, "sqlite error: {e}"),
            Error::Fmt(e) => write!(f, "fmt error: {e}"),
            Error::KeySerialize(e) => write!(f, "key serialize error: {e}"),
            Error::KeyDeserialize(e) => write!(f, "key deserialize error: {e}"),
            Error::ValueSerialize(e) => write!(f, "value serialize error: {e}"),
            Error::ValueDeserialize(e) => write!(f, "value deserialize error: {e}"),
        }
    }
}

impl<K, V> std::error::Error for Error<K, V>
where
    K: Format,
    V: Format,
{
}
