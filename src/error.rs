use crate::db::{SetVersionError, SetupError};

use std::fmt;

#[derive(Debug)]
pub enum OpenError {
    Sqlite(rusqlite::Error),
    Fmt(std::fmt::Error),
    ApplicationId(i32),
    TableVersion(i64),
    UserVersion(i32),
    TableType { expected: String, actual: String },
}

impl From<std::fmt::Error> for OpenError {
    fn from(v: std::fmt::Error) -> Self {
        Self::Fmt(v)
    }
}

impl From<rusqlite::Error> for OpenError {
    fn from(v: rusqlite::Error) -> Self {
        Self::Sqlite(v)
    }
}

impl From<SetupError> for OpenError {
    fn from(v: SetupError) -> Self {
        match v {
            SetupError::Sqlite(e) => OpenError::Sqlite(e),
            SetupError::ApplicationId(e) => OpenError::ApplicationId(e),
            SetupError::UserVersion(e) => OpenError::UserVersion(e),
            SetupError::TableType { expected, actual } => OpenError::TableType { expected, actual },
        }
    }
}
impl From<SetVersionError> for OpenError {
    fn from(v: SetVersionError) -> Self {
        match v {
            SetVersionError::Sqlite(e) => OpenError::Sqlite(e),
        }
    }
}

impl fmt::Display for OpenError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            OpenError::Sqlite(e) => write!(f, "sqlite error: {e}"),
            OpenError::Fmt(e) => write!(f, "fmt error: {e}"),
            OpenError::ApplicationId(id) => write!(f, "wrong application id: {id}"),
            OpenError::UserVersion(version) => write!(f, "illegal user version: {version}"),
            OpenError::TableVersion(version) => write!(f, "illegal table version: {version}"),
            OpenError::TableType { expected, actual } => {
                write!(
                    f,
                    "table was wrong type: expected={expected} actual={actual}"
                )
            }
        }
    }
}

impl std::error::Error for OpenError {}
