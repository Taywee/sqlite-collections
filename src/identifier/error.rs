use std::fmt;

#[derive(Debug)]
pub enum Error {
    NullCharacter,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "found illegal null character in identifier")
    }
}

impl std::error::Error for Error {}
